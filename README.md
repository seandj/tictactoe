This is a program that I wrote, as a programming test, for a job that I applied for.

It uses RESTful services, that run on a Tomcat Server. The test was a service to validate a Tic Tac Toe board. I wrote the JavaScript front end, to allow the testing of the services.

A working version is at http://yellowzipper.net:8080/TicTacToe.

Later I wrote a service, and front end to allow playing a game. You can get to that page at http://yellowzipper.net:8080/TicTacToe/play.html

There is also a help page at http://yellowzipper.net:8080/TicTacToe/help.html
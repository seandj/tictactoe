package net.yellowzipper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.Math;

import com.json.exceptions.JSONParsingException;
import com.json.parsers.JSONParser;
import com.json.parsers.JsonParserFactory;
@Path("solver")
public class Solver {
	
	private int[][] winners = 
	{
		{1, 2, 3}, // Across top 
		{4, 5, 6}, // Across middle
		{7, 8, 9}, // Across bottom
		{1, 4, 7}, // Down left
		{2, 5, 8}, // Down middle
		{3, 6, 9}, // Down right
		{1, 5, 9}, // Diaginal left to right
		{3, 5, 7}  // Diaginal right to left
	};
	
	final String NO_WINNER = "No Winner";
	final String X_WINS    = "X Wins";
	final String O_WINS    = "O Wins";
	final String SUCCESS   = "Success";
	
	private String buildResponse(String state, int status, String message) {
		return "{\"state\":\""+state+
				"\",\"status\":\""+Integer.toString(status)+
				"\",\"message\":\""+message+"\"}";
	}
		
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String solver(InputStream incomingData) {
		
		String result = null;
		StringBuilder JsonBuilder = new StringBuilder();
		try {
			
			// Grab the content from the request
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				JsonBuilder.append(line);
			}
			result = JsonBuilder.toString();
			
			if (result.trim().isEmpty()) {
				return buildResponse(NO_WINNER, 8, "Request string is empty");
			}
			
			// Fix the empty strings, since they are illegal in JSON
			result = result.replaceAll("\"\"", "\" \"");
			result = result.replaceAll("''", "' '");
			
			// Parse the JSON into a map
			JsonParserFactory factory=JsonParserFactory.getInstance();
			JSONParser parser=factory.newJsonParser();
			Map jsonMap=parser.parseJson(result);
			
			// Get the rows from the map.
			ArrayList row1 = (ArrayList)jsonMap.get("row1");
			ArrayList row2 = (ArrayList)jsonMap.get("row2");
			ArrayList row3 = (ArrayList)jsonMap.get("row3");
			
			
			// Validate that we have all three rows
			if (row1 == null || row2 == null || row3 == null) {
				return buildResponse(NO_WINNER, 6, "Some rows are missing");
			}
			
			// Validate that we have only 3 elements in each row. We ignore
			// rows that are not 1 - 3			
			if (row1.size() != 3 || row2.size() != 3 || row3.size() != 3) {
				return buildResponse(NO_WINNER, 5, "Some rows do not contain 3 values");
			}
			
			// Create one big array list for the rows, to work easier with them.
			ArrayList<String> playField=new ArrayList<String>();
			playField.addAll(row1);
			playField.addAll(row2);
			playField.addAll(row3);
			
			// Validate that we have only Xs, Os, or empty string			
			int xValues = 0;
			int oValues = 0;
			boolean success = true;
			for (String value : playField) {
				value = value.toUpperCase();
				if (!("XO ".indexOf(value.trim()) != -1 || value.trim().isEmpty()) ) {
					success = false;
				}
				else {
					if (value.equals("X")) {xValues++;}
					if (value.equals("O")) {oValues++;}
				}
			}
			
			if (!success) {
				return buildResponse(NO_WINNER, 4, "Some values are not valid");
			}
						
			// Validate that we have the correct number of Xs and Os.  Impossible result
			if (Math.abs(xValues - oValues) > 1) {
				return buildResponse(NO_WINNER, 3, "Not a valid game");
			}
						
			// Determine who is the winner
			String value = null;
			boolean haveWinner = false;
			int xWins = 0;
			int oWins = 0;
			result =  buildResponse(NO_WINNER, 0, SUCCESS);
		    for(int i=0; i <winners.length; i++) {
		    	xValues = 0;
		    	oValues = 0;
		        for(int j=0; j<winners[i].length; j++) {
		        	value = playField.get(winners[i][j]-1).toUpperCase();
					if (value.equals("X")) {xValues++;}
					if (value.equals("O")) {oValues++;}
		        }
		        if (xValues == 3) {
		        	xWins++;
		        }
		        if (oValues == 3) {
		        	oWins++;
		        }
		    }
		    if (xWins > 0 && oWins > 0) {
	        	result =  buildResponse(NO_WINNER, 7, "Both players seem to have won, Invalid board");
		    }
		    else if (xWins > 0) {
	        	result =  buildResponse(X_WINS, 0, SUCCESS);
		    }
		    else if (oWins > 0) {
	        	result =  buildResponse(O_WINS, 0, SUCCESS); 
		    }

		} catch (JSONParsingException e) {
			// Catches any parsing exception
			
			result =  buildResponse(NO_WINNER, 2, "Error Parsing: - "+e.getMessage());
		} catch (Exception e) {
			// Catches all other excrptions
			result =  buildResponse(NO_WINNER, 1, "Exception: - "+e.getMessage());
		}		
				
		return result;
	}
}



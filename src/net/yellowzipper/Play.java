package net.yellowzipper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.Math;

import com.json.exceptions.JSONParsingException;
import com.json.parsers.JSONParser;
import com.json.parsers.JsonParserFactory;
@Path("play")
public class Play {
	int make2(String board) {
		int value = 5;
		while(board.charAt(value-1) != ' ') {
			value = 2 + 2*ThreadLocalRandom.current().nextInt((8-2)/2+1);
		}
		return value;
	}
	
	int anyplace(String board) {
		int value = ThreadLocalRandom.current().nextInt(1, 9 + 1);
		while(board.charAt(value-1) != ' ') {
			value = ThreadLocalRandom.current().nextInt(1, 9 + 1);
		}
		return value;
	}

	private int[][] winners = 
	{
		{1, 2, 3}, // Across top 
		{4, 5, 6}, // Across middle
		{7, 8, 9}, // Across bottom
		{1, 4, 7}, // Down left
		{2, 5, 8}, // Down middle
		{3, 6, 9}, // Down right
		{1, 5, 9}, // Diaginal left to right
		{3, 5, 7}  // Diaginal right to left
	};
	
	int possWin(String board, char player) {
		return possWin(board, player, false);
	}

	int possWin(String board, char player, boolean complete) {
		int result = 0;
		char value = ' ';
		boolean haveWinner = false;
		int values;
		int openSpace = -1;
	    for(int i=0; i <winners.length; i++) {
	    	values = 0;
	        for(int j=0; j<winners[i].length; j++) {
	        	value = Character.toUpperCase(board.charAt(winners[i][j]-1));
				if (value == player) {values+=2;}
				if (value == ' ') {values+=1; openSpace = winners[i][j]-1;}
	        }
	        if (complete && values == 6) {
	        	return 10;
	        }
	        if (!complete && values == 5) {
	        	result = openSpace + 1;
	        	break;
	        }
	    }
		return result;
	}

	char boardPos(String board, int pos) {
		return board.charAt(pos - 1);
	}

	String go(String board, char player, int pos) {
		char[] chars = board.toCharArray();
		chars[pos-1] = player;
		return new String(chars);		
	}
	
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String play(InputStream incomingData) {
		PlayResponse pr = new PlayResponse();
		String result = null;
		StringBuilder JsonBuilder = new StringBuilder();
		try {
			
			// Grab the content from the request
			BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
			String line = null;
			while ((line = in.readLine()) != null) {
				JsonBuilder.append(line);
			}
			result = JsonBuilder.toString();
			
			if (result.trim().isEmpty()) {
				pr.setBoard(null);
				pr.setStatus(8);
				pr.setMessage("Request string is empty");
				return pr.toJsonString();
			}
			
			// Fix the empty strings, since they are illegal in JSON
			result = result.replaceAll("\"\"", "\" \"");
			result = result.replaceAll("''", "' '");
			
			// Parse the JSON into a map
			JsonParserFactory factory=JsonParserFactory.getInstance();
			JSONParser parser=factory.newJsonParser();
			Map jsonMap=parser.parseJson(result);
			
			String board = (String)jsonMap.get("board");
			String player = (String)jsonMap.get("player");
			pr.setBoard(board);
			pr.setPlayer(player);
						
			// Validate that we have only 3 elements in each row. We ignore
			// rows that are not 1 - 3			
			if (board.length()!=9) {
				pr.setStatus(5);
				pr.setMessage("Board does not have 9 values");
				return pr.toJsonString();
			}
			
			// Validate that we have only Xs, Os, or empty string
			int xValues = 0;
			int oValues = 0;
			boolean success = true;
			for (char value : board.toCharArray()) {
				value = Character.toUpperCase(value);
				if (!("XO ".indexOf(value) != -1) ) {
					success = false;
				}
				else {
					if (value == 'X') {xValues++;}
					if (value == 'O') {oValues++;}
				}
			}
			
			if (!success) {
				pr.setStatus(4);
				pr.setMessage("Some values are not valid");
				return pr.toJsonString();
			}
						
			// Validate that we have the correct number of Xs and Os.  Impossible result
			if (Math.abs(xValues - oValues) > 1) {
				pr.setStatus(3);
				pr.setMessage("Not a valid game");
				return pr.toJsonString();
			}
			
			switch (xValues+oValues+1) {
			case 6:
				if (possWin(board, 'X', true) == 10) {
					pr.setState("W");
					pr.setPlayer("X");
				}
				break;
			case 7:
				if (possWin(board, 'O', true) == 10) {
					pr.setState("W");
					pr.setPlayer("O");
				}
				break;
			case 8:
				if (possWin(board, 'X', true) == 10) {
					pr.setState("W");
					pr.setPlayer("X");
				}
				break;
			case 9:
				if (possWin(board, 'O', true) == 10) {
					pr.setState("W");
					pr.setPlayer("O");
				}
				else {
					pr.setState("T");
					pr.setPlayer("O");
				}
				break;
			}
			
			if (!pr.getState().equals("N")) {
				result = pr.toJsonString();
			}
			else {
				// Determine next move
				
				// Values added together plus 1 is the turn
				char charPlayer = player.charAt(0);
				switch (xValues+oValues+1) {
				case 1: // X
					// Initial move
					board = go(board, charPlayer, 1);
					break;
				case 2: // O
					// Center if available, otherwize top left
					if (boardPos(board, 5) == ' ') {
						board = go(board, charPlayer, 5);
					}
					else {
						board = go(board, charPlayer, 1);
					}
					break;
				case 3: // X
					// Bottom left if available, otherize top right
					if (boardPos(board, 9) == ' ') {
						board = go(board, charPlayer, 9);
					}
					else {
						board = go(board, charPlayer, 3);
					}
					break;
				case 4: // O
					// Try to block X, otherwise pick a empty space
					if (possWin(board, 'X') != 0) {
						board = go(board, charPlayer, possWin(board, 'X'));
						pr.setState("B");
					}
					else {
						//board = go(board, charPlayer, make2(board));
						board = go(board, charPlayer, anyplace(board));
					}
					break;
				case 5: // X
					// X to win, otherwise block O, otherwise bottom left, 
					// otherise top right, otherwise any open space
					if (possWin(board, 'X') != 0) {
						board = go(board, charPlayer, possWin(board, 'X'));
						pr.setState("W");
					}
					else if (possWin(board, 'O') != 0) {
						board = go(board, charPlayer, possWin(board, 'O'));
						pr.setState("B");
					}
					else if (boardPos(board, 7) == ' ') {
						board = go(board, charPlayer, 7);
					}
					else if (boardPos(board, 3) == ' ') {
						board = go(board, charPlayer, 3);
					}
					else {
						for (int i = 1; i <=9; i++) {
							if (boardPos(board, i) == ' ') {
								board = go(board, charPlayer, i);
								break;
							}
						}
					}
					break;
				case 6: // O
					if (possWin(board, 'O') != 0) {
						board = go(board, charPlayer, possWin(board, 'O'));
						pr.setState("W");
					}
					else if (possWin(board, 'X') != 0) {
						board = go(board, charPlayer, possWin(board, 'X'));
						pr.setState("B");
					}
					else {
						board = go(board, charPlayer, make2(board));
					}
					break;
				case 7: // X
				case 9: // X
					if (possWin(board, 'X') != 0) {
						board = go(board, charPlayer, possWin(board, 'X'));
						pr.setState("W");
					}
					else if (possWin(board, 'O') != 0) {
						board = go(board, charPlayer, possWin(board, 'O'));
						pr.setState("B");
					}
					else {
						for (int i = 1; i <=9; i++) {
							if (boardPos(board, i) == ' ') {
								board = go(board, charPlayer, i);
								break;
							}
						}
					}
					break;
				case 8: // O
					if (possWin(board, 'O') != 0) {
						board = go(board, charPlayer, possWin(board, 'O'));
						pr.setState("W");
					}
					else if (possWin(board, 'X') != 0) {
						board = go(board, charPlayer, possWin(board, 'X'));
						pr.setState("B");
					}
					else {
						for (int i = 1; i <=9; i++) {
							if (boardPos(board, i) == ' ') {
								board = go(board, charPlayer, i);
								break;
							}
						}
					}
					if (possWin(board,'X') == 0) {
						pr.setState("T");
					}
					break;
				};
				pr.setBoard(board);
				result = pr.toJsonString();
			}

		} catch (JSONParsingException e) {
			// Catches any parsing exception
			
			pr.setStatus(2);
			pr.setMessage("Error Parsing: - "+e.getMessage());
			result = pr.toJsonString();
		} catch (Exception e) {
			// Catches all other excrptions
			pr.setStatus(1);
			pr.setMessage("Exception: - "+e.getMessage());
			result = pr.toJsonString();
		}		
				
		return result;
	}
}

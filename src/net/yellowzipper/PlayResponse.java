package net.yellowzipper;

public class PlayResponse {
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getBoard() {
		return board;
	}
	public void setBoard(String board) {
		this.board = board;
	}
	
	public String getPlayer() {
		return player;
	}
	
	public void setPlayer(String player) {
		this.player = player;
	}

	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
		
	public String toJsonString() {
		String result = "{";
		result += "\"board\":" + ((board == null)?"         ":"\""+board+"\"")+",";
		result += "\"status\":" + Integer.toString(status)+",";
		result += "\"message\":\"" +message + "\",";
		result += "\"state\":\""+state+"\",";
		result += "\"player\":\""+player+"\"";
		result += "}";
		return result;
	}
	
	private int status = 0;
	private String message = "Success";
	private String board = null;
	private String player = "X";
	private String state = "N";
}
